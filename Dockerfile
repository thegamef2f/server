FROM debian:10 as build

WORKDIR /home/thegamef2f

RUN apt-get update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get -y install build-essential ninja-build cmake cmake-curses-gui cppcheck valgrind lcov \
    && apt-get -y install clang clang-format clang-tidy clang-tools llvm lldb \
    && apt-get -y install python3-pip \
    && pip3 install conan cpplint \
    && CC=gcc CXX=g++ conan profile new default --detect \
    && conan profile update settings.compiler.libcxx=libstdc++11 default \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

COPY . .

RUN mkdir build \
    && cmake -S . -B build \
    && cmake --build build

FROM debian:10

WORKDIR /home/thegamef2f
COPY --from=build /home/thegamef2f/build/bin/TheGameF2FServer .
ENTRYPOINT [ "./TheGameF2FServer" ]