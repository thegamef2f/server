#define BOOST_TEST_MODULE Game
#include <boost/test/included/unit_test.hpp>

#include <TheGameF2F/Game.hpp>
using TheGameF2F::Card;
using TheGameF2F::Cards;
using TheGameF2F::Game;
using TheGameF2F::IPlayer;
using Pile = IPlayer::Pile;
using Piles = IPlayer::Piles;
using Color = Game::Color;
using Side = Game::Side;

class TestPlayer : public IPlayer {
public:
    using IPlayer::Pile;
    using IPlayer::Piles;
    TestPlayer(Cards drawPile, Cards handCards, Piles piles)
        : m_drawPile{drawPile}, m_handCards{handCards}, m_piles{piles} {}
    void init() {}
    const Cards &getHandCards() const { return m_handCards; }
    const Piles &getPiles() const { return m_piles; }
    bool playCard(const Card &id) {
        auto it = std::find(m_handCards.begin(), m_handCards.end(), id);
        if (it == m_handCards.end()) return false;
        m_handCards.erase(it);
        return true;
    }
    bool putCardOnPile(const Card &card, const Pile &pile) {
        if (pile == Pile::Down)
            m_piles.down = card;
        else
            m_piles.up = card;
        return true;
    }
    bool drawCards() {
        while (m_handCards.size() < 6 && !m_drawPile.empty()) {
            m_handCards.push_back(m_drawPile.back());
            m_drawPile.pop_back();
        }
        std::sort(std::begin(m_handCards), std::end(m_handCards));
        return true;
    }
    bool drawCards(std::size_t num) {
        while (num-- && !m_drawPile.empty()) {
            m_handCards.push_back(m_drawPile.back());
            m_drawPile.pop_back();
        }
        std::sort(std::begin(m_handCards), std::end(m_handCards));
        return true;
    }
    std::size_t getDrawPileSize() const { return m_drawPile.size(); }

private:
    Cards m_drawPile;
    Cards m_handCards;
    Piles m_piles;
};

namespace std {
std::ostream &operator<<(std::ostream &os, const Color c) {
    if (c == Color::Gold) return os << "Gold";
    return os << "Silver";
}
}  // namespace std

BOOST_AUTO_TEST_CASE(initially_no_winner) {
    Cards drawPile1(50);
    std::iota(drawPile1.begin(), drawPile1.end(), 10);
    Cards handCards1 = {4, 5, 6, 7, 8, 9};
    Piles piles1{60, 3};
    std::unique_ptr<IPlayer> p1 = std::make_unique<TestPlayer>(drawPile1, handCards1, piles1);
    Cards drawPile2(50);
    std::iota(drawPile2.begin(), drawPile2.end(), 10);
    Cards handCards2 = {2, 3, 4, 5, 6, 7};
    Piles piles2{60, 3};
    std::unique_ptr<IPlayer> p2 = std::make_unique<TestPlayer>(drawPile2, handCards2, piles2);
    const auto game = Game(std::move(p1), std::move(p2));
    BOOST_TEST(game.getActivePlayerColor() == Color::Gold);
    BOOST_TEST(!game.getWinner().has_value());
}

BOOST_AUTO_TEST_CASE(no_winner_after_move) {
    Cards drawPile1(50);
    std::iota(drawPile1.begin(), drawPile1.end(), 10);
    Cards handCards1 = {4, 5, 6, 7, 8, 9};
    Piles piles1{60, 3};
    std::unique_ptr<IPlayer> p1 = std::make_unique<TestPlayer>(drawPile1, handCards1, piles1);
    Cards drawPile2(50);
    std::iota(drawPile2.begin(), drawPile2.end(), 10);
    Cards handCards2 = {4, 5, 6, 7, 8, 9};
    Piles piles2{60, 3};
    std::unique_ptr<IPlayer> p2 = std::make_unique<TestPlayer>(drawPile2, handCards2, piles2);
    auto game = Game(std::move(p1), std::move(p2));
    BOOST_TEST(game.playCard(game.getHandCards(Color::Gold)[4], Side::Player, Pile::Up));
    BOOST_TEST(game.getActivePlayerColor() == Color::Gold);
    BOOST_TEST(!game.getWinner().has_value());
}

BOOST_AUTO_TEST_CASE(winner_after_draw_with_two_moves) {
    Cards drawPile1(50);
    std::iota(drawPile1.begin(), drawPile1.end(), 10);
    Cards handCards1 = {4, 5, 6, 7, 8, 9};
    Piles piles1{59, 3};
    std::unique_ptr<IPlayer> p1 = std::make_unique<TestPlayer>(drawPile1, handCards1, piles1);
    Cards drawPile2(50);
    std::iota(drawPile2.begin(), drawPile2.end(), 10);
    Cards handCards2 = {27, 29, 34, 46, 59};
    Piles piles2{11, 55};
    std::unique_ptr<IPlayer> p2 = std::make_unique<TestPlayer>(drawPile2, handCards2, piles2);
    auto game = Game(std::move(p1), std::move(p2));
    BOOST_TEST(game.getActivePlayerColor() == Color::Gold);
    BOOST_TEST(game.playCard(game.getHandCards(Color::Gold)[0], Side::Player, Pile::Up));
    BOOST_TEST(game.playCard(game.getHandCards(Color::Gold)[0], Side::Player, Pile::Up));
    BOOST_TEST(!game.getWinner().has_value());
    BOOST_TEST(game.drawCards());
    BOOST_TEST(game.getActivePlayerColor() == Color::Silver);
    BOOST_TEST(game.getWinner().has_value());
    BOOST_TEST(game.getWinner().value() == Color::Gold);
}
