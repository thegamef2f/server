#include <TheGameF2F/Game.hpp>

#include <algorithm>
#include <iterator>

namespace {
using TheGameF2F::Cards;
using TheGameF2F::maxCard;
using Piles = TheGameF2F::Player::Piles;

bool isMovesPossible(std::size_t moves, Piles plPiles, Piles opPiles, Cards cards, bool opPile) {
    if (moves == 0) return true;
    for (std::size_t i = 0; i < cards.size(); ++i) {
        auto newCards = cards;
        auto it = newCards.begin();
        std::advance(it, i);
        newCards.erase(it);
        if (!opPile && cards[i] > opPiles.down) {
            const auto piles = Piles{cards[i], opPiles.up};
            if (isMovesPossible(moves - 1, plPiles, piles, cards, true)) return true;
        }
        if (!opPile && cards[i] < opPiles.up) {
            const auto piles = Piles{opPiles.down, cards[i]};
            if (isMovesPossible(moves - 1, plPiles, piles, cards, true)) return true;
        }
        if (cards[i] < plPiles.down || (plPiles.down < maxCard - 10 && cards[i] == plPiles.down + 10)) {
            const auto piles = Piles{cards[i], plPiles.up};
            if (isMovesPossible(moves - 1, piles, opPiles, cards, opPile)) return true;
        }
        if (cards[i] > plPiles.up || (plPiles.up > 10 && cards[i] == plPiles.up - 10)) {
            const auto piles = Piles{plPiles.down, cards[i]};
            if (isMovesPossible(moves - 1, piles, opPiles, cards, opPile)) return true;
        }
    }
    return false;
}

}  // namespace

namespace TheGameF2F {

Game::Game()
    : m_randomNumberGenerator(m_randomDevice()),
      m_players{std::make_unique<Player>(m_randomNumberGenerator), std::make_unique<Player>(m_randomNumberGenerator)} {}
Game::Game(std::unique_ptr<IPlayer> &&p1, std::unique_ptr<IPlayer> &&p2)
    : m_randomNumberGenerator(m_randomDevice()), m_players{std::move(p1), std::move(p2)} {}

void Game::init() {
    m_cardsPlayed = 0;
    m_playedOnOpponentsPile = false;
    m_activePlayerColor = Color::Gold;
    m_players[static_cast<int>(Color::Silver)]->init();
    m_players[static_cast<int>(Color::Gold)]->init();
    m_winnerColor.reset();
}

bool Game::playCard(Card card, Side side, Player::Pile pile) {
    if (m_winnerColor) return false;
    auto &activePlayer = getActivePlayer();
    const auto &handCards = activePlayer.getHandCards();
    if (std::find(handCards.cbegin(), handCards.cend(), card) == handCards.cend()) return false;
    if (side == Side::Opponent && m_playedOnOpponentsPile) return false;
    if (side == Side::Player) {
        const auto &piles = activePlayer.getPiles();
        if (pile == Player::Pile::Down && card >= piles.down && card != piles.down + 10) return false;
        if (pile == Player::Pile::Up && card <= piles.up && card != piles.up - 10) return false;
    } else {
        const auto &piles = getNonActivePlayer().getPiles();
        if (pile == Player::Pile::Down && card < piles.down) return false;
        if (pile == Player::Pile::Up && card > piles.up) return false;
    }

    if (side == Side::Opponent) m_playedOnOpponentsPile = true;
    ++m_cardsPlayed;

    if (side == Side::Player) {
        activePlayer.putCardOnPile(card, pile);
    } else {
        getNonActivePlayer().putCardOnPile(card, pile);
    }
    activePlayer.playCard(card);

    if (checkWin()) {
        m_winnerColor.emplace(m_activePlayerColor);
    }
    if (checkLoss()) {
        m_winnerColor.emplace(getOpponentColor());
    }
    return true;
}

bool Game::drawCards() {
    if (m_winnerColor) return false;
    if (m_cardsPlayed < 2) return false;
    auto &activePlayer = getActivePlayer();
    if (m_playedOnOpponentsPile)
        activePlayer.drawCards();
    else
        activePlayer.drawCards(2);
    m_cardsPlayed = 0;
    m_playedOnOpponentsPile = false;
    m_activePlayerColor = getOpponentColor();
    if (checkLoss()) {
        m_winnerColor.emplace(getOpponentColor());
    }
    return true;
}

bool Game::checkWin() const {
    const auto &activePlayer = getActivePlayer();
    return activePlayer.getHandCards().size() == 0 && activePlayer.getDrawPileSize() == 0;
}

bool Game::checkLoss() const {
    if (m_cardsPlayed >= 2) return false;
    return !isMovesPossible(2 - m_cardsPlayed, getActivePlayer().getPiles(), getNonActivePlayer().getPiles(),
                            getActivePlayer().getHandCards(), m_playedOnOpponentsPile);
}

Game::Color Game::getOpponentColor() const {
    if (m_activePlayerColor == Color::Gold)
        return Color::Silver;
    else
        return Color::Gold;
}

}  // namespace TheGameF2F
