#include <TheGameF2F/Player.hpp>

#include <algorithm>
#include <numeric>

namespace TheGameF2F {

void Player::init() {
    m_discardPile.up = 1;
    m_discardPile.down = 60;
    m_drawPile.resize(58);
    std::iota(std::begin(m_drawPile), std::end(m_drawPile), 2);
    std::shuffle(std::begin(m_drawPile), std::end(m_drawPile), m_randomNumberGenerator);

    m_handCards.clear();
    m_handCards.resize(6);
    for (auto &handCard : m_handCards) {
        handCard = m_drawPile.back();
        m_drawPile.pop_back();
    }
    std::sort(std::begin(m_handCards), std::end(m_handCards));
}

const Cards &Player::getHandCards() const { return m_handCards; }

bool Player::playCard(const Card &id) {
    auto it = std::find(m_handCards.begin(), m_handCards.end(), id);
    if (it == m_handCards.end()) return false;
    m_handCards.erase(it);
    return true;
}

bool Player::putCardOnPile(const Card &card, const Pile &pile) {
    if (pile == Pile::Down)
        m_discardPile.down = card;
    else
        m_discardPile.up = card;
    return true;
}

bool Player::drawCards() {
    while (m_handCards.size() < 6 && !m_drawPile.empty()) {
        m_handCards.push_back(m_drawPile.back());
        m_drawPile.pop_back();
    }
    std::sort(std::begin(m_handCards), std::end(m_handCards));
    return true;
}

bool Player::drawCards(std::size_t num) {
    while (num-- && !m_drawPile.empty()) {
        m_handCards.push_back(m_drawPile.back());
        m_drawPile.pop_back();
    }
    std::sort(std::begin(m_handCards), std::end(m_handCards));
    return true;
}

}  // namespace TheGameF2F
