#pragma once

#include <cstdint>
#include <random>
#include <vector>

namespace TheGameF2F {
using Card = std::uint32_t;
using Cards = std::vector<Card>;
constexpr Card maxCard = 60;

class IPlayer {
public:
    enum class Pile { Down, Up };
    struct Piles {
        Card down;
        Card up;
    };
    virtual void init() = 0;
    virtual const Cards &getHandCards() const = 0;
    virtual const Piles &getPiles() const = 0;
    virtual bool playCard(const Card &id) = 0;
    virtual bool putCardOnPile(const Card &card, const Pile &pile) = 0;
    virtual bool drawCards() = 0;
    virtual bool drawCards(std::size_t num) = 0;
    virtual std::size_t getDrawPileSize() const = 0;

    virtual ~IPlayer() = default;
};

class Player : public IPlayer {
public:
    using IPlayer::Pile;
    using IPlayer::Piles;
    Player(std::mt19937 &randomNumberGenerator) : m_randomNumberGenerator(randomNumberGenerator) {}
    void init() override;
    const Cards &getHandCards() const override;
    const Piles &getPiles() const override { return m_discardPile; }
    bool playCard(const Card &id) override;
    bool putCardOnPile(const Card &card, const Pile &pile) override;
    bool drawCards() override;
    bool drawCards(std::size_t num) override;
    std::size_t getDrawPileSize() const override { return m_drawPile.size(); }

private:
    Cards m_drawPile;
    Cards m_handCards;
    Piles m_discardPile;
    std::mt19937 &m_randomNumberGenerator;
};

}  // namespace TheGameF2F
