#pragma once

#include "Player.hpp"

#include <array>
#include <memory>
#include <optional>
#include <random>

namespace TheGameF2F {

class Game {
public:
    enum class Color { Gold = 0, Silver };
    enum class Side { Opponent, Player };

    Game();
    Game(std::unique_ptr<IPlayer> &&p1, std::unique_ptr<IPlayer> &&p2);
    void init();
    const Cards &getHandCards(Color color) const { return m_players[static_cast<std::size_t>(color)]->getHandCards(); }
    Color getActivePlayerColor() const { return m_activePlayerColor; }
    const TheGameF2F::Player::Piles &getPiles(const TheGameF2F::Game::Color &color) const {
        return m_players[static_cast<std::size_t>(color)]->getPiles();
    }
    bool playCard(Card card, Side side, Player::Pile pile);
    bool drawCards();
    std::size_t getDrawPileSize(Color color) const {
        return m_players[static_cast<std::size_t>(color)]->getDrawPileSize();
    }
    std::optional<Color> getWinner() const { return m_winnerColor; }

private:
    IPlayer &getActivePlayer() { return *(m_players[static_cast<std::size_t>(m_activePlayerColor)]); }
    const IPlayer &getActivePlayer() const { return *(m_players[static_cast<std::size_t>(m_activePlayerColor)]); }
    IPlayer &getNonActivePlayer() { return *(m_players[(static_cast<std::size_t>(m_activePlayerColor) + 1) % 2]); }
    const IPlayer &getNonActivePlayer() const {
        return *(m_players[(static_cast<std::size_t>(m_activePlayerColor) + 1) % 2]);
    }

    bool checkWin() const;
    bool checkLoss() const;
    Color getOpponentColor() const;
    std::random_device m_randomDevice;
    std::mt19937 m_randomNumberGenerator;
    std::array<std::unique_ptr<IPlayer>, 2> m_players;
    Color m_activePlayerColor = Color::Gold;
    bool m_playedOnOpponentsPile = false;
    std::size_t m_cardsPlayed = 0;
    std::optional<Color> m_winnerColor;
};

}  // namespace TheGameF2F
