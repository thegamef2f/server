#include "Application.hpp"

#include "GameManager.hpp"
#include "RequestHandlerFactory.hpp"

#include <Poco/Formatter.h>
#include <Poco/FormattingChannel.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/PatternFormatter.h>
#include <Poco/Util/Option.h>
#include <Poco/Util/OptionCallback.h>

#include <numeric>
#include <sstream>
#include <string>
#include <vector>

using std::string_literals::operator""s;

namespace Wombytes {

void Application::initialize(Poco::Util::Application &self) {
    loadConfiguration();
    config().setString("logging.loggers.root.channel.class", "ConsoleChannel");
    config().setString("logging.loggers.root.channel.pattern", "[%Y/%m/%d-%H:%M:%S] %s [%p]: %t");
    Poco::Util::Application::initialize(self);
}

void Application::uninitialize() { Poco::Util::Application::uninitialize(); }

void Application::reinitialize(Poco::Util::Application &self) { Poco::Util::Application::reinitialize(self); }

void Application::defineOptions(Poco::Util::OptionSet &optionSet) {
    Poco::Util::Application::defineOptions(optionSet);
    static auto debugCallback = [this](const std::string &, const std::string &) mutable {
        config().setString("logging.loggers.root.level", "debug");
    };
    Poco::Util::OptionCallback<decltype(debugCallback)> cb(&debugCallback, &decltype(debugCallback)::operator());
    optionSet.addOption(Poco::Util::Option("debug", "d", "Sets root logger level to debug").noArgument().callback(cb));
}

void Application::handleOption(const std::string &name, const std::string &value) {
    Poco::Util::Application::handleOption(name, value);
}

int Application::main(const std::vector<std::string> & /*args*/) {
    GameManager gameManager;

    Poco::UInt16 port = static_cast<Poco::UInt16>(config().getInt("Server.port", 8080));
    logger().debug("Server started on port %hu", port);

    Poco::Net::HTTPServer srv(new RequestHandlerFactory(gameManager), port);
    srv.start();
    waitForTerminationRequest();
    srv.stop();
    return Poco::Util::Application::EXIT_OK;
}

}  // namespace Wombytes
