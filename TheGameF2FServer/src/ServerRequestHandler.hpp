#pragma once

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

namespace Wombytes {

class ServerRequestHandler : public Poco::Net::HTTPRequestHandler {
public:
    ServerRequestHandler(const std::uint64_t requestCounter);
    void handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) override;

private:
    const std::uint64_t m_requestCounter;

    void setLogLevel(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);
    void getLogLevel(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);
};

}  // namespace Wombytes
