#pragma once

#include "GameManager.hpp"

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

namespace Wombytes {

class GameRequestHandler : public Poco::Net::HTTPRequestHandler {
public:
    GameRequestHandler(GameManager &gameManager, const std::uint64_t requestCounter);
    void handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) override;

private:
    GameManager &m_gameManager;
    const std::uint64_t m_requestCounter;

    void getStatus(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);
    void playCard(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);
    void drawCards(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);
    void reset(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);
};

}  // namespace Wombytes
