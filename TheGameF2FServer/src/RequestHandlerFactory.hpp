#pragma once

#include "GameManager.hpp"

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerRequest.h>

namespace Wombytes {

class RequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {
public:
    explicit RequestHandlerFactory(GameManager &gameManager);
    Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request) override;

private:
    GameManager &m_gameManager;
    std::atomic_uint64_t m_requestCounter{0};
};

}  // namespace Wombytes
