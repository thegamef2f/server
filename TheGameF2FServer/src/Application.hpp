#pragma once

#include <TheGameF2F/Game.hpp>

#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Util/OptionSet.h>
#include <Poco/Util/ServerApplication.h>

#include <string>
#include <vector>

namespace Wombytes {

class Application : public Poco::Util::ServerApplication {
    void initialize(Poco::Util::Application &self);
    void uninitialize();
    void reinitialize(Poco::Util::Application &self);
    void defineOptions(Poco::Util::OptionSet &optionSet) override;
    void handleOption(const std::string &name, const std::string &value) override;
    int main(const std::vector<std::string> &args) override;
};

}  // namespace Wombytes
