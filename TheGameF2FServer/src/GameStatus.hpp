#pragma once

#include <TheGameF2F/Game.hpp>

#include <optional>
#include <string>

struct GameStatus {
    bool active;
    const TheGameF2F::Game::Color playerColor;
    const TheGameF2F::Game::Color opponentColor;
    const std::string playerName;
    const std::string opponentName;
    const std::size_t numOpponentCards;
    const TheGameF2F::Cards playerCards;
    const TheGameF2F::Player::Piles pilesPlayer;
    const TheGameF2F::Player::Piles pilesOpponent;
    const std::size_t drawPilePlayerSize;
    const std::size_t drawPileOpponentSize;
    const std::optional<TheGameF2F::Game::Color> winnerColor;
};
