#include "ErrorRequestHandler.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/URI.h>
#include <Poco/Util/Application.h>

#include <fmt/format.h>

#include <exception>
#include <string>

namespace Wombytes {

ErrorRequestHandler::ErrorRequestHandler(const std::uint64_t requestCounter) : m_requestCounter(requestCounter) {}

void ErrorRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                        Poco::Net::HTTPServerResponse &response) {
    const Poco::URI uri(request.getURI());
    Poco::Util::Application::instance().logger().debug("[Request: %lu] 404 - HTTP Not Found", m_requestCounter);
    response.setContentType("text/html");
    response.setStatusAndReason(Poco::Net::HTTPResponse::HTTPStatus::HTTP_NOT_FOUND);
    response.send();
}

}  // namespace Wombytes
