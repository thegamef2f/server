#include "RequestHandlerFactory.hpp"

#include "ErrorRequestHandler.hpp"
#include "GameRequestHandler.hpp"
#include "ServerRequestHandler.hpp"

#include <Poco/URI.h>
#include <Poco/Util/Application.h>

#include <string>
#include <vector>

namespace Wombytes {

RequestHandlerFactory::RequestHandlerFactory(GameManager &gameManager) : m_gameManager(gameManager) {}

Poco::Net::HTTPRequestHandler *RequestHandlerFactory::createRequestHandler(
    const Poco::Net::HTTPServerRequest &request) {
    const auto requestCounter = m_requestCounter++;
    auto uri = Poco::URI(request.getURI());
    Poco::Util::Application::instance().logger().debug("[Request: %lu] %s", requestCounter, uri.toString());
    std::vector<std::string> pathSegments;
    uri.getPathSegments(pathSegments);
    if (pathSegments.size() != 4 || pathSegments[0] != "api" || pathSegments[1] != "v1") {
        return new ErrorRequestHandler(requestCounter);
    }
    if (pathSegments[2] == "game") return new GameRequestHandler(m_gameManager, requestCounter);
    if (pathSegments[2] == "server") return new ServerRequestHandler(requestCounter);
    return new ErrorRequestHandler(requestCounter);
}

}  // namespace Wombytes
