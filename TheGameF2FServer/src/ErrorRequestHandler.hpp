#pragma once

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

namespace Wombytes {

class ErrorRequestHandler : public Poco::Net::HTTPRequestHandler {
public:
    ErrorRequestHandler(const std::uint64_t requestCounter);
    void handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) override;

private:
    const std::uint64_t m_requestCounter;
};

}  // namespace Wombytes
