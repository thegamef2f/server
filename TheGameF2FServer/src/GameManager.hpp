#pragma once

#include "GameStatus.hpp"

#include <TheGameF2F/Game.hpp>

#include <string>
#include <unordered_map>

class GameManager {
public:
    GameManager();
    GameStatus getStatus(const std::string &sessionId);
    bool playCard(const std::string &sessionId, const TheGameF2F::Card &card, const TheGameF2F::Game::Side &side,
                  const TheGameF2F::Player::Pile &pile);
    bool drawCards(const std::string &sessionId);
    void reset(const std::string &sessionId);

private:
    using Games = std::unordered_map<std::size_t, TheGameF2F::Game>;
    Games m_games;
};
