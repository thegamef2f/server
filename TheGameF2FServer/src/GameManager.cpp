#include "GameManager.hpp"

#include <optional>

namespace {

TheGameF2F::Game::Color getPlayerColorFromSessionId(const std::string &sessionId) {
    if (sessionId == "gold") return TheGameF2F::Game::Color::Gold;
    if (sessionId == "silver") return TheGameF2F::Game::Color::Silver;
    throw std::exception();
}

TheGameF2F::Game::Color getOpponentColorFromPlayerColor(const TheGameF2F::Game::Color &playerColor) {
    if (playerColor == TheGameF2F::Game::Color::Gold) return TheGameF2F::Game::Color::Silver;
    if (playerColor == TheGameF2F::Game::Color::Silver) return TheGameF2F::Game::Color::Gold;
    throw std::exception();
}

std::string getNameFromColor(const TheGameF2F::Game::Color &color) {
    if (color == TheGameF2F::Game::Color::Gold) return "Gold";
    if (color == TheGameF2F::Game::Color::Silver) return "Silver";
    throw std::exception();
}

}  // namespace

GameManager::GameManager() : m_games(1) {
    auto &game = m_games[0];
    game.init();
}

GameStatus GameManager::getStatus(const std::string &sessionId) {
    const auto &game = m_games[0];
    const auto playerColor = getPlayerColorFromSessionId(sessionId);
    const auto activePlayerColor = game.getActivePlayerColor();
    const auto opponentColor = getOpponentColorFromPlayerColor(playerColor);
    const auto playerName = getNameFromColor(playerColor);
    const auto opponentName = getNameFromColor(opponentColor);
    const auto active = activePlayerColor == playerColor;
    const auto numOpponentCards = game.getHandCards(opponentColor).size();
    const auto playerCards = game.getHandCards(playerColor);
    const auto pilesPlayer = game.getPiles(playerColor);
    const auto pilesOpponent = game.getPiles(opponentColor);
    const auto drawPilePlayerSize = game.getDrawPileSize(playerColor);
    const auto drawPileOpponentSize = game.getDrawPileSize(opponentColor);
    const auto winnerColor = game.getWinner();
    return GameStatus{active,        playerColor,        opponentColor,        playerName,
                      opponentName,  numOpponentCards,   playerCards,          pilesPlayer,
                      pilesOpponent, drawPilePlayerSize, drawPileOpponentSize, winnerColor};
}

bool GameManager::playCard(const std::string &, const TheGameF2F::Card &card, const TheGameF2F::Game::Side &side,
                           const TheGameF2F::Player::Pile &pile) {
    auto &game = m_games[0];
    return game.playCard(card, side, pile);
}

bool GameManager::drawCards(const std::string &) {
    auto &game = m_games[0];
    return game.drawCards();
}

void GameManager::reset(const std::string &) {
    auto &game = m_games[0];
    game.init();
}
