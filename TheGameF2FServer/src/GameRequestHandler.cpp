#include "GameRequestHandler.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/URI.h>
#include <Poco/Util/Application.h>

#include <fmt/format.h>

#include <exception>
#include <string>
#include <vector>

namespace Wombytes {

namespace {

class MissingMandatoryQueryParameterException : public std::exception {
public:
    MissingMandatoryQueryParameterException(const Poco::URI &uri, const std::string &key)
        : msg{fmt::format("Missing key \"{}\" in request \"{}\"", key, uri.toString())} {}
    const char *what() const noexcept override { return msg.c_str(); }

private:
    const std::string msg;
};

std::string loadMandatoryQueryParameter(const Poco::URI &uri, const std::string &key) {
    const auto queryParameters = uri.getQueryParameters();
    const auto it = std::find_if(std::begin(queryParameters), std::end(queryParameters),
                                 [&key](const auto &param) { return param.first == key; });
    if (it == std::end(queryParameters)) throw Wombytes::MissingMandatoryQueryParameterException(uri, key);
    return it->second;
}

TheGameF2F::Game::Side sideStringToSide(const std::string &sideString) {
    if (sideString == "opponent") {
        return TheGameF2F::Game::Side::Opponent;
    } else if (sideString == "player") {
        return TheGameF2F::Game::Side::Player;
    }
    throw std::exception();
}

TheGameF2F::Player::Pile pileStringToPile(const std::string &pileString) {
    if (pileString == "up") {
        return TheGameF2F::Player::Pile::Up;
    } else if (pileString == "down") {
        return TheGameF2F::Player::Pile::Down;
    }
    throw std::exception();
}

TheGameF2F::Card cardStringToCard(const std::string &cardString) {
    const auto cardValue = std::stoi(cardString);
    if (cardValue >= 1 && cardValue <= 60) return static_cast<TheGameF2F::Card>(cardValue);
    throw std::exception();
}

std::string colorToString(const TheGameF2F::Game::Color &color) {
    if (color == TheGameF2F::Game::Color::Gold) return "Gold";
    if (color == TheGameF2F::Game::Color::Silver) return "Silver";
    throw std::exception();
}

}  // namespace

GameRequestHandler::GameRequestHandler(GameManager &gameManager, const std::uint64_t requestCounter)
    : m_gameManager(gameManager), m_requestCounter(requestCounter) {}

void GameRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
    Poco::URI uri(request.getURI());
    std::vector<std::string> pathSegments;
    uri.getPathSegments(pathSegments);
    try {
        if (request.getMethod() == "GET" && pathSegments[3] == "getStatus") {
            getStatus(request, response);
        } else if (request.getMethod() == "GET" && pathSegments[3] == "playCard") {
            playCard(request, response);
        } else if (request.getMethod() == "GET" && pathSegments[3] == "drawCards") {
            drawCards(request, response);
        } else if (request.getMethod() == "GET" && pathSegments[3] == "reset") {
            reset(request, response);
        } else {
            Poco::Util::Application::instance().logger().debug("[Request: %lu] 404 - HTTP Not Found", m_requestCounter);
            response.setContentType("text/html");
            response.setStatusAndReason(Poco::Net::HTTPResponse::HTTPStatus::HTTP_NOT_FOUND);
            response.send();
        }
    } catch (std::exception &e) {
        Poco::Util::Application::instance().logger().debug("[Request: %lu] 400 - HTTP Bad Request", m_requestCounter);
        Poco::Util::Application::instance().logger().debug("[Request: %lu] %s", m_requestCounter,
                                                           std::string(e.what()));
        response.setContentType("text/html");
        response.setStatusAndReason(Poco::Net::HTTPResponse::HTTPStatus::HTTP_BAD_REQUEST);
        response.send();
    }
}

void GameRequestHandler::getStatus(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
    const Poco::URI uri(request.getURI());
    const auto sessionId = loadMandatoryQueryParameter(uri, "sessionId");
    const auto gameStatus = m_gameManager.getStatus(sessionId);
    Poco::JSON::Object pilesPlayer;
    pilesPlayer.set("down", gameStatus.pilesPlayer.down);
    pilesPlayer.set("up", gameStatus.pilesPlayer.up);
    Poco::JSON::Object pilesOpponent;
    pilesOpponent.set("down", gameStatus.pilesOpponent.down);
    pilesOpponent.set("up", gameStatus.pilesOpponent.up);
    Poco::JSON::Object status;
    status.set("active", gameStatus.active);
    status.set("playerColor", colorToString(gameStatus.playerColor));
    status.set("opponentColor", colorToString(gameStatus.opponentColor));
    status.set("playerName", gameStatus.playerName);
    status.set("opponentName", gameStatus.opponentName);
    status.set("numOpponentCards", gameStatus.numOpponentCards);
    status.set("playerCards", gameStatus.playerCards);
    status.set("pilesPlayer", pilesPlayer);
    status.set("pilesOpponent", pilesOpponent);
    status.set("drawPilePlayerSize", gameStatus.drawPilePlayerSize);
    status.set("drawPileOpponentSize", gameStatus.drawPileOpponentSize);
    if (gameStatus.winnerColor.has_value())
        status.set("winnerColor", colorToString(gameStatus.winnerColor.value()));
    else
        status.set("winnerColor", Poco::Dynamic::Var());
    response.setContentType("application/json");
    status.stringify(response.send());
}

void GameRequestHandler::playCard(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
    const Poco::URI uri(request.getURI());
    const auto sessionId = loadMandatoryQueryParameter(uri, "sessionId");
    const auto card = cardStringToCard(loadMandatoryQueryParameter(uri, "card"));
    const auto side = sideStringToSide(loadMandatoryQueryParameter(uri, "side"));
    const auto pile = pileStringToPile(loadMandatoryQueryParameter(uri, "pile"));
    if (!m_gameManager.playCard(sessionId, card, side, pile)) {
        throw std::exception();
    }
    response.setContentType("text/plain");
    response.send();
}

void GameRequestHandler::drawCards(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
    const Poco::URI uri(request.getURI());
    const auto sessionId = loadMandatoryQueryParameter(uri, "sessionId");
    if (!m_gameManager.drawCards(sessionId)) {
        throw std::exception();
    }
    response.setContentType("text/plain");
    response.send();
}

void GameRequestHandler::reset(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
    const Poco::URI uri(request.getURI());
    const auto sessionId = loadMandatoryQueryParameter(uri, "sessionId");
    m_gameManager.reset(sessionId);
    response.setContentType("text/plain");
    response.send();
}

}  // namespace Wombytes
