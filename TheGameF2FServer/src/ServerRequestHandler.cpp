#include "ServerRequestHandler.hpp"

#include <Poco/JSON/Object.h>
#include <Poco/URI.h>
#include <Poco/Util/Application.h>

#include <fmt/format.h>

#include <exception>
#include <optional>
#include <string>
#include <vector>

namespace Wombytes {

namespace {

class MissingMandatoryQueryParameterException : public std::exception {
public:
    MissingMandatoryQueryParameterException(const Poco::URI &uri, const std::string &key)
        : msg{fmt::format("Missing key \"{}\" in request \"{}\"", key, uri.toString())} {}
    const char *what() const noexcept override { return msg.c_str(); }

private:
    const std::string msg;
};

class WrongQueryParameterValueException : public std::exception {
public:
    WrongQueryParameterValueException(const Poco::URI &uri, const std::string &key, const std::string &value)
        : msg{fmt::format("Value \"{}\" for key \"{}\" in request \"{}\" not accepted", value, key, uri.toString())} {}
    const char *what() const noexcept override { return msg.c_str(); }

private:
    const std::string msg;
};

std::string loadMandatoryQueryParameter(const Poco::URI &uri, const std::string &key) {
    const auto queryParameters = uri.getQueryParameters();
    const auto it = std::find_if(std::begin(queryParameters), std::end(queryParameters),
                                 [&key](const auto &param) { return param.first == key; });
    if (it == std::end(queryParameters)) throw Wombytes::MissingMandatoryQueryParameterException(uri, key);
    return it->second;
}

std::optional<std::string> loadOptionalQueryParameter(const Poco::URI &uri, const std::string &key) {
    const auto queryParameters = uri.getQueryParameters();
    const auto it = std::find_if(std::begin(queryParameters), std::end(queryParameters),
                                 [&key](const auto &param) { return param.first == key; });
    if (it == std::end(queryParameters)) return {};
    return it->second;
}

}  // namespace

ServerRequestHandler::ServerRequestHandler(const std::uint64_t requestCounter) : m_requestCounter(requestCounter) {}

void ServerRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                         Poco::Net::HTTPServerResponse &response) {
    Poco::URI uri(request.getURI());
    std::vector<std::string> pathSegments;
    uri.getPathSegments(pathSegments);
    try {
        if (request.getMethod() == "GET" && pathSegments[3] == "getLogLevel") {
            getLogLevel(request, response);
        } else if (request.getMethod() == "GET" && pathSegments[3] == "setLogLevel") {
            setLogLevel(request, response);
        } else {
            Poco::Util::Application::instance().logger().debug("[Request: %lu] 404 - HTTP Not Found", m_requestCounter);
            response.setContentType("text/html");
            response.setStatusAndReason(Poco::Net::HTTPResponse::HTTPStatus::HTTP_NOT_FOUND);
            response.send();
        }
    } catch (std::exception &e) {
        Poco::Util::Application::instance().logger().debug("[Request: %lu] 400 - HTTP Bad Request", m_requestCounter);
        Poco::Util::Application::instance().logger().debug("[Request: %lu] %s", m_requestCounter,
                                                           std::string(e.what()));
        response.setContentType("text/html");
        response.setStatusAndReason(Poco::Net::HTTPResponse::HTTPStatus::HTTP_BAD_REQUEST);
        response.send();
    }
}

void ServerRequestHandler::getLogLevel(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
    const Poco::URI uri(request.getURI());
    const auto name = loadOptionalQueryParameter(uri, "name");
    Poco::JSON::Object obj;
    if (name) {
        std::vector<std::string> names;
        Poco::Logger::names(names);

        if (std::find(names.begin(), names.end(), name) == names.end()) {
            throw WrongQueryParameterValueException(uri, "name", *name);
        }
        obj.set("level", Poco::Logger::get(*name).getLevel());
    } else {
        obj.set("level", Poco::Util::Application::instance().logger().getLevel());
    }
    response.setContentType("application/json");
    obj.stringify(response.send());
}

void ServerRequestHandler::setLogLevel(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
    const Poco::URI uri(request.getURI());
    const auto name = loadOptionalQueryParameter(uri, "name");
    const auto level = loadMandatoryQueryParameter(uri, "level");
    Poco::JSON::Object obj;
    if (name) {
        std::vector<std::string> names;
        Poco::Logger::names(names);

        if (std::find(names.begin(), names.end(), name) == names.end()) {
            throw WrongQueryParameterValueException(uri, "name", *name);
        }
        Poco::Logger::setLevel(*name, Poco::Logger::parseLevel(level));
    } else {
        Poco::Util::Application::instance().logger().setLevel(level);
    }
    response.setContentType("text/plain");
    response.send();
}

}  // namespace Wombytes
